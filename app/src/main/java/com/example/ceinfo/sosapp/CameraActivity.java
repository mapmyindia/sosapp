package com.example.ceinfo.sosapp;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        String mUsername=getIntent().getStringExtra("username");
        if (null == savedInstanceState) {
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, Camera2VideoFragment.newInstance(mUsername))
                        .commit();
            }else{
                Log.i(Constants.TAG, "API level less than Lollipop");
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, CameraFragment.newInstance(mUsername))
                        .commit();
            }
        }
    }
}
