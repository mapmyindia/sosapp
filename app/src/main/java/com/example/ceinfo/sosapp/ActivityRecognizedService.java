package com.example.ceinfo.sosapp;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

/**
 * Created by ce on 01-Jun-16.
 */
public class ActivityRecognizedService extends IntentService{

    int delay;
    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(Constants.TAG, "onHandleIntent");
        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities(result.getProbableActivities());
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        //TODO: set time
        switch(probableActivities.get(0).getType()) {
            case DetectedActivity.IN_VEHICLE: {
                Log.i(Constants.TAG, "In Vehicle: " + probableActivities.get(0).getConfidence());
                delay= 10*1000;
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                Log.i(Constants.TAG, "On Bicycle: " + probableActivities.get(0).getConfidence());
                delay= 15*1000;
                break;
            }
            case DetectedActivity.ON_FOOT: {
                Log.i(Constants.TAG, "On Foot: " + probableActivities.get(0).getConfidence());
                delay= 25*1000;
                break;
            }
            case DetectedActivity.RUNNING: {
                Log.i(Constants.TAG, "Running: " + probableActivities.get(0).getConfidence());
                delay= 20*1000;
                break;
            }
            case DetectedActivity.STILL: {
                Log.i(Constants.TAG, "Still: " + probableActivities.get(0).getConfidence());
                delay= 30*1000;
                break;
            }
            case DetectedActivity.TILTING: {
                Log.i(Constants.TAG, "Tilting: " + probableActivities.get(0).getConfidence());
                delay= 25*1000;
                break;
            }
            case DetectedActivity.WALKING: {
                Log.i(Constants.TAG, "Walking: " + probableActivities.get(0).getConfidence());
                delay= 20*1000;
                break;
            }
            case DetectedActivity.UNKNOWN: {
                Log.i(Constants.TAG, "Unknown: " + probableActivities.get(0).getConfidence());
                delay= 25*1000;
                break;
            }
        }
        Intent another = new Intent(Constants.UPDATE_BROADCAST);
        another.putExtra(Constants.UPDATE_MESSAGE, Constants.FIX_DELAY);
        another.putExtra(Constants.UPDATE_DELAY, delay);
        sendBroadcast(another);
    }
}

