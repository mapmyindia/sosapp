package com.example.ceinfo.sosapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class signup extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Button signup = (Button) findViewById(R.id.button2);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText first_name = (EditText) findViewById(R.id.first_name);
                EditText last_name = (EditText) findViewById(R.id.last_name);
                EditText username = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                EditText phone_number = (EditText) findViewById(R.id.phone_number);
                final String username1 = username.getText().toString();
                final String password1 = password.getText().toString();
                final String first_name1 = first_name.getText().toString();
                final String last_name1 = last_name.getText().toString();
                final String phone_number1 = phone_number.getText().toString();
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                CustomRequest customRequest = new CustomRequest(
                        Request.Method.POST,
                        Constants.base_url+"users/add",
                        new Response.Listener<String>(){

                            /**
                             * if successful go to main activity screen
                             * otherwise display appropriate message
                             * @param response
                             */
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getBoolean("success")){
                                        SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
                                        editor.putString(CustomRequest.SESSION_COOKIE, null);
                                        editor.commit();
                                        startActivity(new Intent(signup.this, LoginActivity.class));

                                    }else{
                                        onCreate(savedInstanceState);
                                        Log.w(Constants.TAG, "Wrong info provided");
                                        Toast.makeText(signup.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    //not expected to reach here
                                    Log.e(Constants.TAG, e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener(){

                            /**
                             * display error
                             * @param error
                             */
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                //handle volley errors
                                if (error!=null){
                                    Log.w(Constants.TAG, error.getMessage()+"");
                                }else{
                                    Log.w(Constants.TAG, "error came to be null");
                                }

                                if (error instanceof TimeoutError){
                                    Log.w(Constants.TAG, "Connection time out");
                                    Toast.makeText(signup.this, "Taking too long to connect", Toast.LENGTH_SHORT).show();
                                }else if (error instanceof NoConnectionError){
                                    Log.w(Constants.TAG, "No Connection error");
                                    Toast.makeText(signup.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                                }else if (error instanceof AuthFailureError) {
                                    Log.w(Constants.TAG, "Authorization failure");
                                    Toast.makeText(signup.this, "Authorization failed", Toast.LENGTH_SHORT).show();
                                } else if (error instanceof ServerError) {
                                    Log.e(Constants.TAG, "Server failure");
                                    Toast.makeText(signup.this, "Server error occurred", Toast.LENGTH_SHORT).show();
                                } else if (error instanceof NetworkError) {
                                    Log.w(Constants.TAG, "Network failure");
                                    Toast.makeText(signup.this, "Network error occurred", Toast.LENGTH_SHORT).show();
                                } else if (error instanceof ParseError) {
                                    Log.e(Constants.TAG, "Parse failure");
                                    Toast.makeText(signup.this, "Parse error occurred", Toast.LENGTH_SHORT).show();
                                }
                            }
                        },
                        signup.this
                ){
                    //send parameters through post method
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("FirstName", first_name1);
                        params.put("LastName", last_name1);
                        params.put("username", username1);
                        params.put("password", password1);
                        params.put("phone", phone_number1);
                        return params;
                    }
                };
                //add to request queue
                requestQueue.add(customRequest);
            }
        });
    }
}
