package com.example.ceinfo.sosapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by CEINFO on 27-05-2016.
 * TODO: determine constants to optimise performance and battery balance
 * TODO: if both network and GPS are off
 * TODO: check if location updates are coming or not
 * TODO: Correct parameters for sensor
 * TODO: send info when network connectivity available
 */
public class NotificationService extends Service
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SensorEventListener{

    /**
     * checked if client connected for
     * --request location updates
     * --disconnect
     * --remove location updates
     */

    private NotificationCompat.Builder mBuilder;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private DBHelper myDB;
    private UpdateReceiver updateReceiver;
    boolean mRequestingLocationUpdates = false;
    private GPSTracker gps;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private int delay = 3000;
    private boolean playServicesAvailable;
    SensorManager mSensorManager;
    Sensor mSensor;
    private long last_update = 0;
    private double last_x, last_y, last_z;
    private static final int STILL_THRESHOLD = 10;
    private static final int WALKING_THRESHOLD = 400;
    private static final int RUNNING_THRESHOLD = 800;
    static Location mLastLocation;
    private boolean mClientConnected = false;

    public boolean isGooglePlayServicesAvailable(Context activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            Log.i(Constants.TAG, "API services Unavailable");
            return false;
        }
        Log.i(Constants.TAG, "API services available");
        return true;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * initialised APIClient, LocationRequest
     * regularly check if settings available
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();

        if(isGooglePlayServicesAvailable(this)) {

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(ActivityRecognition.API)
                    .build();

            mLocationRequest = new LocationRequest()
                    .setInterval(10000)
                    .setFastestInterval(10000)
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            playServicesAvailable = true;

        }else{

            gps = new GPSTracker(this);

            playServicesAvailable = false;
        }

        updateReceiver = new UpdateReceiver();

        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent another = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, another, 0);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent == null || intent.getAction().equals(Constants.START_SERVICE)) {

            Log.i(Constants.TAG, "Start service");

            if(intent!=null){
                myDB = new DBHelper(this, intent.getStringExtra(Constants.USERNAME));
            }

            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
            editor.putBoolean(Constants.SERVICE_STATUS, true);
            editor.commit();


            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.UPDATE_BROADCAST);
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(updateReceiver, intentFilter);

            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction(Constants.MAIN_ACTION);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);

            mBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Current Location")
                    .setTicker("Current Location")
                    .setContentText("your location")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true);

            startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());

            if(playServicesAvailable){
                if (mClientConnected) {

                    Log.i(Constants.TAG, "Already connected");
                    //TODO: what if updates already present
                    startLocationUpdates();

                    if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED){
                        Log.i(Constants.TAG, "Permission not present");
                        Intent other = new Intent(Constants.BROADCAST_ACTION);
                        other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                        sendBroadcast(other);
                    }else{
                        Log.i(Constants.TAG, "Got last location");
                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    }

                } else {
                    Log.i(Constants.TAG, "Connecting Client...");
                    mGoogleApiClient.connect();
                }
            }else{
                Log.i(Constants.TAG, "Services unavailable");
                mLastLocation = gps.mLastLocation;
                if(!gps.getLocation()){
                    Intent other = new Intent(Constants.BROADCAST_ACTION);
                    other.putExtra(Constants.MESSAGE, Constants.LOCATION_REQUEST);
                    sendBroadcast(other);
                }
            }

            if(playServicesAvailable && Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                if(mClientConnected){
                    startActivityUpdates();
                }
            }else{
                mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                mSensorManager.registerListener(this, mSensor, 30*1000*1000);
            }

            if(mLastLocation!=null){
                Log.i(Constants.TAG, "First location not null");
                myDB.addLocation(mLastLocation);
                sendToDb();
                mBuilder.setContentText("Co-ordinates:" + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude());
                startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
            }
        }else if (intent.getAction().equals(Constants.STOP_SERVICE)) {

            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS, MODE_PRIVATE).edit();
            editor.putBoolean(Constants.SERVICE_STATUS, false);
            editor.commit();

            try{
                unregisterReceiver(updateReceiver);
            }catch (IllegalArgumentException e){
                Log.e(Constants.TAG, "Trying to unregister an unregistered receiver");
            }

            if(playServicesAvailable){
                if (mClientConnected && mRequestingLocationUpdates) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            mGoogleApiClient, this
                    ).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(@NonNull Status status) {
                                    mRequestingLocationUpdates = false;
                                    Log.d(Constants.TAG, "Removed location updates!");
                                }
                            }
                    );

                    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
                        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(
                                mGoogleApiClient, pendingIntent
                        );
                    }else{
                        mSensorManager.unregisterListener(this, mSensor);
                    }

                    mGoogleApiClient.disconnect();
                    mClientConnected = false;
                }
            }else{
                Log.i(Constants.TAG, "Stopping service");
                gps.stopUsingGPS();
                mSensorManager.unregisterListener(this, mSensor);
            }
            stopForeground(true);
            stopSelf();
        }
        return START_STICKY;
    }

    /**
     * request location updates if permission present
     */
    public void startLocationUpdates() {
        Log.i(Constants.TAG, "startLocationUpdates");
        if(mClientConnected && !mRequestingLocationUpdates){
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.i(Constants.TAG, "Permission not present");
                Intent other = new Intent(Constants.BROADCAST_ACTION);
                other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                sendBroadcast(other);
            }else{
                Log.i(Constants.TAG, "Requesting Location Updates");
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this
                ).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                mRequestingLocationUpdates = true;
                                Log.d(Constants.TAG, "Started location updates!");
                            }
                        }
                );
            }
        }
    }

    /**
     * callback method after connecting
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.i(Constants.TAG, "OnConnected");
        mClientConnected = true;

        startActivityUpdates();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            Log.i(Constants.TAG, "Permission not present");
            Intent other = new Intent(Constants.BROADCAST_ACTION);
            other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
            sendBroadcast(other);
        }else{
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation!=null){
                Log.i(Constants.TAG, "Last location not null");
                updateLocation(mLastLocation);
            }else{
                //TODO in case its null
            }
        }
        startLocationUpdates();
    }

    public void startActivityUpdates(){
        Log.i(Constants.TAG, "startActivityUpdates");
        Intent intent = new Intent(this, ActivityRecognizedService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mGoogleApiClient, 3*1000, pendingIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i(Constants.TAG, "Permission not present");
        mClientConnected = false;
        //TODO avoid repeating re-connections
//        Log.d(Constants.TAG, "Connection suspended: "+i);
        if (mRequestingLocationUpdates){
            Log.i(Constants.TAG, "Permission not present");
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this
            ).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            mRequestingLocationUpdates = false;
                            Log.d(Constants.TAG, "Removed location updates!");
                        }
                    }
            );
        }

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mGoogleApiClient, pendingIntent);
        }

        mBuilder.setContentTitle("Connection suspended");
        mBuilder.setContentText("Reconnecting in few moments...");
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            //Not reached
            e.printStackTrace();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(Constants.TAG, "onLocationChanged");
        mLastLocation=location;
        updateLocation(location);
    }

    public void updateLocation(Location location){

        Toast.makeText(NotificationService.this, "location changed", Toast.LENGTH_SHORT).show();
        mBuilder.setContentText("Co-ordinates:" + location.getLatitude() + "," + location.getLongitude());
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        myDB.addLocation(location);
        sendToDb();

        if (mClientConnected && mRequestingLocationUpdates){

            Log.i(Constants.TAG, "Permission not present");

            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this
            ).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            mRequestingLocationUpdates = false;
                            Log.d(Constants.TAG, "Removed Location Updates...");
                        }
                    }
            );

            Log.i(Constants.TAG, "Alarm set for "+delay);
            alarmManager.set(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime()+delay,
                    pendingIntent
            );
        }else{
            Log.i(Constants.TAG, "Permission not present");
        }
    }

    public void updateLocationUsingGPS(Location location) {
        Log.i(Constants.TAG, "Permission not present");
        mBuilder.setContentText("Co-ordinates:" + location.getLatitude() + "," + location.getLongitude());
        startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        myDB.addLocation(location);
        sendToDb();

        Log.i(Constants.TAG, "Permission not present");
        alarmManager.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime()+delay,
                pendingIntent
        );
    }

    public void sendToDb() {
        Log.i(Constants.TAG, "checking if network available...");
        ArrayList<String[]> locations = myDB.getAllLocations();
        Log.d(Constants.TAG, String.valueOf(locations.size()));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        for (int i = 0; i < locations.size(); i++) {
            final String[] details = locations.get(i);
            final int id = Integer.parseInt(details[8]);
            CustomRequest customRequest = new CustomRequest(
                    Request.Method.POST,
                    Constants.base_url + "location/add",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i(Constants.TAG, "Send to db: "+response);
                            myDB.updateStatus(id);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            //handle volley errors
                            if (error!=null){
                                Log.w(Constants.TAG, error.getMessage()+" :Empty message");
                            }else{
                                Log.w(Constants.TAG, "error came to be null");
                            }

                            if (error instanceof TimeoutError){
                                Log.w(Constants.TAG, "Connection time out");
                                Toast.makeText(NotificationService.this, "Taking too long to connect", Toast.LENGTH_SHORT).show();
                            }else if (error instanceof NoConnectionError){
                                Log.w(Constants.TAG, "No Connection error");
                                Toast.makeText(NotificationService.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                            }else if (error instanceof AuthFailureError) {
                                Log.w(Constants.TAG, "Authorization failure");
                                Toast.makeText(NotificationService.this, "Authorization failed", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Log.e(Constants.TAG, "Server failure");
                                Toast.makeText(NotificationService.this, "Server error occurred", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof NetworkError) {
                                Log.w(Constants.TAG, "Network failure");
                                Toast.makeText(NotificationService.this, "Network error occurred", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Log.e(Constants.TAG, "Parse failure");
                                Toast.makeText(NotificationService.this, "Parse error occurred", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, NotificationService.this) {

                @Override
                protected Map<String, String> getParams() {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("longitude", details[0]);
                    params.put("latitude", details[1]);
                    if(details[2]!=null){
                        params.put("altitude", details[2]);
                    }
                    if(details[3]!=null){
                        params.put("accuracy", details[3]);
                    }
                    if(details[4]!=null){
                        params.put("bearing", details[4]);
                    }
                    if(details[5]!=null){
                        params.put("speed", details[5]);
                    }
                    if(details[6]!=null){
                        params.put("provider", details[6]);
                    }
                    params.put("time", details[7]);
                    return params;
                }
            };
            requestQueue.add(customRequest);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mClientConnected = false;
        //TODO: when connection failed
        Log.e(Constants.TAG, "Connection failed due to: "+connectionResult.getErrorMessage());
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        //TODO: this an accelerometer
        Log.i(Constants.TAG, "onSensorChanged");
        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER){
            double x = event.values[0];
            double y = event.values[1];
            double z = event.values[2];

            long current_time = System.currentTimeMillis();

            if (current_time-last_update>20*1000){
                long diff_time = current_time - last_update;
                last_update = current_time;

                double speed = Math.sqrt((x - last_x)*(x - last_x) + (y-last_y)*(y-last_y) + (z-last_z)*(z-last_z))/diff_time*10000;
                if (speed < STILL_THRESHOLD){
                    delay = 30*1000;
                }else if(speed < WALKING_THRESHOLD){
                    delay = 2*60*1000;
                }else if (speed < RUNNING_THRESHOLD){
                    delay = 45*1000;
                }else{
                    delay = 15*1000;
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //TODO: on accuracy changed
    }

    //TODO: check if location in high accuracy mode
    class UpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int request = intent.getIntExtra(Constants.UPDATE_MESSAGE, -1);
            if (request == Constants.SERVICE_START) {
                Log.i(Constants.TAG, "prompt to request location updates");
                if (playServicesAvailable) {
                    if (mClientConnected) {

                        Log.i(Constants.TAG, "Already connected...");
                        startLocationUpdates();

                        (new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(35 * 1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                if (mRequestingLocationUpdates) {

                                    Log.i(Constants.TAG, "Connection time out...");

                                    checkSettings();

                                    LocationServices.FusedLocationApi.removeLocationUpdates(
                                            mGoogleApiClient, NotificationService.this
                                    ).setResultCallback(
                                            new ResultCallback<Status>() {
                                                @Override
                                                public void onResult(@NonNull Status status) {
                                                    mRequestingLocationUpdates = false;
                                                    Log.d(Constants.TAG, "Removed location updates!");
                                                }
                                            }
                                    );

                                    Log.i(Constants.TAG, "Alarm set for: " + delay);
                                    alarmManager.set(
                                            AlarmManager.ELAPSED_REALTIME,
                                            SystemClock.elapsedRealtime() + delay,
                                            pendingIntent
                                    );
                                }
                            }
                        })).start();
                    } else {
                        Log.i(Constants.TAG, "Permission not present");
                        mGoogleApiClient.connect();
                    }
                } else {
                    Log.i(Constants.TAG, "getLocation function to be called");

                    gps.getLocation();

                    (new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5 * 1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if (gps.mRequestLocation) {
                                gps.stopUsingGPS();
                                Log.i(Constants.TAG, "Connection time out...");

                                if (gps.isNetworkEnabled || gps.isGPSEnabled) {
                                    alarmManager.set(
                                            AlarmManager.ELAPSED_REALTIME,
                                            SystemClock.elapsedRealtime() + delay,
                                            pendingIntent
                                    );
                                } else {
                                    Intent notificationIntent = new Intent(NotificationService.this, MainActivity.class);
                                    notificationIntent.setAction(Constants.MAIN_ACTION);
                                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(NotificationService.this, 0,
                                            notificationIntent, 0);
                                    mBuilder.setContentIntent(pendingIntent)
                                            .setContentTitle("Settings unavailable")
                                            .setContentText("Tap to change settings");
                                    startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
                                }
                            }
                        }
                    })).start();
                }
            } else if (request == Constants.FIX_DELAY) {
                Log.i(Constants.TAG, "Setting delay");
                delay = intent.getIntExtra(Constants.UPDATE_DELAY, -1);
            }

            ConnectivityManager conn =  (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conn.getActiveNetworkInfo();

            if (networkInfo != null) {
                Log.i(Constants.TAG, "Finally here");
                sendToDb();
            }
        }
    }

    public void checkSettings() {

        try {
            if(Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE) != 0){
                alarmManager.set(
                        AlarmManager.ELAPSED_REALTIME,
                        SystemClock.elapsedRealtime() + delay,
                        pendingIntent
                );
                return;
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo == null || !networkInfo.isConnected()){
            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction(Constants.MAIN_ACTION);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);
            mBuilder.setContentIntent(pendingIntent)
                    .setContentTitle("Settings unavailable")
                    .setContentText("Tap to change settings");
            startForeground(Constants.FOREGROUND_SERVICE, mBuilder.build());
        }else{
            alarmManager.set(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + delay,
                    pendingIntent
            );
        }
    }
}