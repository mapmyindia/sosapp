package com.example.ceinfo.sosapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by CEINFO on 20-06-2016.
 */
public class DBHelperVideo extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName1.db";
    String mUsername;

    /**
     * call upon a database
     * set the value of user name
     * @param context
     */
    public DBHelperVideo(Context context, String username) {
        super(context, DATABASE_NAME , null, 1);
        mUsername=username;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //TODO
        db.execSQL(
                "create table video_uri " +
                        "(id INTEGER primary key AUTOINCREMENT, filepath VARCHAR(200), status INTEGER, time BIGINT, username VARCHAR(20)," +
                        "startLatitude DOUBLE, stopLatitude DOUBLE, startLongitude DOUBLE, stopLongitude DOUBLE)"

        );
        Log.d("tag1", "Created table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS video_uri");
        onCreate(db);
    }

    public void addUri(String filepath, Location start, Location stop) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("filepath", filepath);
        if(start!=null&&stop!=null){
            contentValues.put("time", start.getTime());
            contentValues.put("startLatitude", start.getLatitude());
            contentValues.put("stopLatitude", stop.getLatitude());
            contentValues.put("startLongitude", start.getLongitude());
            contentValues.put("stopLongitude", stop.getLongitude());
        }
        contentValues.put("status", 0);
        contentValues.put("username", mUsername);
        db.insert("video_uri", null, contentValues);

    }

    public void updateStatus(int id) {
        Log.d("tag2", "updateStatus: "+id);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update video_uri set status = 1 where id ="+id+"");

    }

    /**
     * output - all locations data, only whose status is 0
     * @return
     */
    public ArrayList<String[]> getAllFilePaths() {
        ArrayList<String[]> array_list = new ArrayList<String[]>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from video_uri where status="+0+" and username like '%" + mUsername + "%'", null);

        res.moveToFirst();

        while(res.isAfterLast() == false){
            String[] data = new String[7];
            data[0]= res.getString(res.getColumnIndex("filepath"));
            data[1]= res.getString(res.getColumnIndex("id"));
            data[2]=res.getString(res.getColumnIndex("time"));
            data[3]=res.getString(res.getColumnIndex("startLatitude"));
            data[4]=res.getString(res.getColumnIndex("stopLatitude"));
            data[5]=res.getString(res.getColumnIndex("startLongitude"));
            data[6]=res.getString(res.getColumnIndex("stopLongitude"));
            array_list.add(data);
            res.moveToNext();
        }

        return array_list;
    }
}
