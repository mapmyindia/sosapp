package com.example.ceinfo.sosapp;

/**
 * Created by ce on 02-Jun-16.
 */

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

public class GPSTracker implements LocationListener {

    private NotificationService mNotificationService;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES_NETWORK = 1; // 10 meters

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES_GPS = 1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES_NETWORK = 1000 * 2; // 1 minute

    private static final long MIN_TIME_BW_UPDATES_GPS = 1000 * 2; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;


    public boolean mRequestLocation = false;

    public Location mLastLocation;

    public GPSTracker(NotificationService mNotificationService) {
        this.mNotificationService = mNotificationService;
        //location manager
        locationManager = (LocationManager) mNotificationService
                .getSystemService(mNotificationService.LOCATION_SERVICE);
        //setting last known location
        if (ContextCompat.checkSelfPermission(mNotificationService, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            Intent other = new Intent(Constants.BROADCAST_ACTION);
            other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
            mNotificationService.sendBroadcast(other);
        }
        if (mLastLocation == null) {
            mLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if(mLastLocation==null){
            mLastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
    }

    public boolean getLocation() {
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(isGPSEnabled){
            Log.i(Constants.TAG, "GPS is enabled");
            if (ContextCompat.checkSelfPermission(mNotificationService, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.w(Constants.TAG, "No permission");
                Intent other = new Intent(Constants.BROADCAST_ACTION);
                other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                mNotificationService.sendBroadcast(other);
            }else{
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES_GPS,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES_GPS,
                        this
                );
                mRequestLocation= true;
                Log.i(Constants.TAG, "Using GPS to get location");
            }
        }else if(isNetworkEnabled){
            if (ContextCompat.checkSelfPermission(mNotificationService, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                Intent other = new Intent(Constants.BROADCAST_ACTION);
                other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                mNotificationService.sendBroadcast(other);
            } else {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES_NETWORK,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES_NETWORK,
                        this
                );
                mRequestLocation = true;
                Log.d("Network", "Network");
            }
        }else{
            return false;
        }
        return true;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS() {

        if (locationManager != null) {
            if (ContextCompat.checkSelfPermission(mNotificationService, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                Intent other = new Intent(Constants.BROADCAST_ACTION);
                other.putExtra(Constants.MESSAGE, Constants.PERMISSION_REQUEST);
                mNotificationService.sendBroadcast(other);
            }else {
                locationManager.removeUpdates(this);
                mRequestLocation=false;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d(Constants.TAG, "Location changed from gps");

        if(location.hasAltitude()){
            Toast.makeText(mNotificationService,
                    "altitude:"+location.getAltitude()+"\n"+
                            "time:"+location.getTime()+"\n"+
                            "accuracy:"+location.getAccuracy()+"\n"+
                            "bearing:"+location.getBearing()+"\n"+
                            "speed:"+location.getSpeed()+"\n",
                    Toast.LENGTH_SHORT).show();
        }
        mNotificationService.updateLocationUsingGPS(location);
        if(mRequestLocation) {
            stopUsingGPS();
        }
        NotificationService.mLastLocation=location;
    }

    @Override
    public void onProviderDisabled(String provider) {
        //TODO:
        Log.i(Constants.TAG, "onProviderDisabled");
    }

    @Override
    public void onProviderEnabled(String provider) {
        //TODO:
        Log.i(Constants.TAG, "onProviderEnabled");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //TODO:
        Log.i(Constants.TAG, "onStatusChanged");
    }
}

