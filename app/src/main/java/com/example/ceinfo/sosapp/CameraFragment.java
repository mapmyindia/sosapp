package com.example.ceinfo.sosapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.location.Location;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.CookieStore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraFragment extends Fragment implements SurfaceHolder.Callback{

    private Camera mCamera;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    boolean status;
    boolean recording = false;
    Bundle bundle;
    private MediaRecorder mediaRecorder;
    private String  fileuri;
    private Button start;
    private Thread thread;
    private DBHelperVideo mydb;
    private static String mUsername;
    Location trigger, stop;

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what)
            {
                case 1 :

                    start.setText("stop");
                    break;
                case 2 :

                    start.setText("start");
                    break;
                default:break;
            }
        }
    };


    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CameraFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CameraFragment newInstance(String username) {
        Log.i(Constants.TAG, "");
        mUsername=username;
        return new CameraFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(Constants.TAG, "");
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View customView, Bundle savedInstanceState) {
        Log.i(Constants.TAG, "");
        super.onViewCreated(customView, savedInstanceState);
        mydb = new DBHelperVideo(customView.getContext(), mUsername);
        surfaceView = (SurfaceView) customView.findViewById(R.id.surfaceView);
        status = false;
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        start = (Button) customView.findViewById(R.id.button);
        chooseCamera();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recording) {
                    stop=NotificationService.mLastLocation;
                    try{
                        mediaRecorder.stop(); // stop the recording
                    releaseMediaRecorder();}
                    catch(RuntimeException e)
                    {

                    }

                    recording = false;
                    thread.interrupt();
                    Toast.makeText(getActivity(), "Video captured!", Toast.LENGTH_LONG).show();
                    (new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("tag2", "Thread from listener!!");
                            launchUploadActivity(fileuri);
                        }
                    })).start();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setAction(Constants.BACK_SOS);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean prepareMediaRecorder() {
        Log.i(Constants.TAG, "");
        mediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
        fileuri = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM) + "/"
                + System.currentTimeMillis() + ".mp4";
        mediaRecorder.setOutputFile(fileuri);
        Log.w("tag2",fileuri);
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private boolean hasCamera(Context context) {

        Log.i(Constants.TAG, "");
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private void launchUploadActivity(String file){

        mydb.addUri(file, trigger, stop);
        sendtodb();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void sendtodb()
    {
        Log.d("tag2", "sendtodb");
        if(isNetworkAvailable())
        {
            ArrayList<String[]> files = mydb.getAllFilePaths();

            Log.d("tag2", String.valueOf(files.size()));

            for(int i =0; i<files.size(); i++)
            {
                String [] result = files.get(i);
                Log.d("tag2",result[0]+"");
                uploadvideo(result);
            }
        }
        else
        {
            Log.w(Constants.TAG, "Authorization failure");
        }

    }


    private void uploadvideo(final String[] data) {

        Log.d("tag2", "uploadVideo");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RequestParams mParams=new RequestParams();
        mParams.put("startLatitude", data[3]);
        mParams.put("stopLatitude", data[4]);
        mParams.put("startLongitude", data[5]);
        mParams.put("stopLongitude", data[6]);
        mParams.put("timestamp", data[2]);
        try {
            mParams.put("image", new File(data[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        SyncHttpClient client = new SyncHttpClient();

        final SharedPreferences _preferences = getActivity().getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);

        String sessionId = _preferences.getString(CustomRequest.SESSION_COOKIE, "");
        if (sessionId.length() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append(CustomRequest.SESSION_COOKIE);
            builder.append("=");
            builder.append(sessionId);
            Log.d(Constants.TAG, "Added header with key: "+CustomRequest.COOKIE_KEY+" and value: "+builder.toString());
            client.addHeader(CustomRequest.COOKIE_KEY, builder.toString());
        }

        client.post(Constants.url, mParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //TODO:
                throwable.printStackTrace();
                Log.d("tag2", "onFailure: "+responseString);
                if(responseString!=null){
                    Log.e("tag2", "Response not null");
                }else{
                    Log.e("tag2", "Response null");
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                //TODO:

                for(int i=0; i<headers.length; i++){
                    if(headers[i].getName().equals(CustomRequest.SET_COOKIE_KEY)){
                        String cookie = headers[i].getValue();
                        if(cookie.startsWith(CustomRequest.SESSION_COOKIE)&&cookie.length()>0){
                            String[] splitCookie = cookie.split(";");
                            String[] splitSessionId = splitCookie[0].split("=");
                            cookie = splitSessionId[1];
                            SharedPreferences.Editor prefEditor = _preferences.edit();
                            Log.d(Constants.TAG, "Stored following cookie: "+cookie);
                            prefEditor.putString(CustomRequest.SESSION_COOKIE, cookie);
                            prefEditor.commit();
                            break;
                        }
                    }
                }



                Log.d("tag2", "onSuccess: "+responseString);
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    if(jsonObject.getBoolean("success")){
                        Log.i("tag2", responseString);
                        int id = Integer.parseInt(data[1]);
                        mydb.updateStatus(id);

                    }else{
                        Log.i("tag2", responseString);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.i(Constants.TAG, e.getMessage());
                }
            }
        });
    }




    public void chooseCamera() {
        Log.i(Constants.TAG, "");
        int cameraId = findBackFacingCamera();
        if (cameraId >= 0)
        {
            mCamera = Camera.open(cameraId);
            mCamera.setDisplayOrientation(90);
            refreshCamera(mCamera);
        }

    }

    public void refreshCamera(Camera camera) {
        Log.i(Constants.TAG, "");
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {
        }

        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {

        }
    }


    private int findBackFacingCamera() {
        Log.i(Constants.TAG, "");
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private void releaseMediaRecorder() {
        Log.i(Constants.TAG, "");
        if (mediaRecorder != null) {
            mediaRecorder.reset(); // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock(); // lock camera for later use
        }
    }

    private void releaseCamera() {
        Log.i(Constants.TAG, "");
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(Constants.TAG, "surfaceCreated");
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            }
        } catch (IOException e) {

        }
        if (!prepareMediaRecorder()) {
            Toast.makeText(getActivity(), "Fail in prepareMediaRecorder()!\n - Ended -", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
        try {
            trigger = NotificationService.mLastLocation;
            mediaRecorder.start();
            handler.sendEmptyMessage(1);
        } catch ( Exception ex) {

        }

        recording = true;
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10*1000);

                    if(recording){
                        try{
                            stop=NotificationService.mLastLocation;
                            mediaRecorder.stop(); // stop the recording
                            releaseMediaRecorder();}
                        catch(RuntimeException e)
                        {

                        }
                        handler.sendEmptyMessage(2);
                        Log.d("tag2", "Thread launchActivity");
                        recording = false;
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.setAction(Constants.BACK_SOS);
                        startActivity(intent);
                        launchUploadActivity(fileuri);

                    }

                } catch (InterruptedException e) {
                    Log.d("tag2", e.getMessage()+"interrupted");
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(Constants.TAG, "surfaceChanged");
        refreshCamera(mCamera);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(Constants.TAG, "surfaceDestroyed");
    }

    @Override
    public void onPause()
    {
        if(recording){
            Log.i(Constants.TAG, "onPause");
            try{
                stop=NotificationService.mLastLocation;
                mediaRecorder.stop(); // stop the recording
            }
            catch(RuntimeException e)
            {

            }
            Log.d("tag2", "onPause launch upload activity");
            recording = false;
            thread.interrupt();
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.setAction(Constants.BACK_SOS);
            startActivity(intent);
            launchUploadActivity(fileuri);
        }
        releaseCamera();
        status = true;
        super.onPause();
    }

    public void onResume() {
        Log.i(Constants.TAG, "onResume");
        super.onResume();
        if (!hasCamera(getActivity())) {
            Toast toast = Toast.makeText(getActivity(), "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("tag2", "oNDestroy");
        getActivity().finish();
    }
}
